const Joi = require("@hapi/joi");

const validateUserLogin = user => {
  const schema = Joi.object({
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required()
  });
  return Joi.validate(user, schema);
};

export default validateUserLogin;
