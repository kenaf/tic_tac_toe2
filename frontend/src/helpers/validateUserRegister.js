const Joi = require("@hapi/joi");

const validateUserRegister = user => {
  const schema = Joi.object().keys({
    email: Joi.string()
      .min(5)
      .max(255)
      .required()
      .email(),
    password: Joi.string()
      .min(5)
      .max(255)
      .required(),
    confirmPassword: Joi.string()
      .required()
      .valid(Joi.ref("password"))
      .options({
        language: {
          any: {
            allowOnly: "Passwords do not match !!"
          }
        }
      })
  });
  return Joi.validate(user, schema);
};

export default validateUserRegister;
