import axios from "axios";
import { userConstants } from "../constants/userConstants";
import validateUserLogin from "../helpers/validateUserLogin";
import validateUserRegister from "../helpers/validateUserRegister";
import { history } from "../helpers/history";
import jwt from "jwt-decode";

const loginSuccess = payload => ({ type: userConstants.LOGINSUCCESS, payload });
const loginError = payload => ({ type: userConstants.LOGINERROR, payload });
const registerError = payload => ({ type: userConstants.REGISTERERROR, payload });

const handleStorage = token => {
  localStorage.setItem("token", token);
  history.push("/");
};

export const logout = () => ({ type: userConstants.LOGOUT });

export const loginUser = userInfo => dispatch => {
  const { email, password } = userInfo;
  const { error } = validateUserLogin({ email, password });
  if (error) {
    dispatch(loginError(error.details[0].message));
  } else {
    const URL = "http://localhost:3002/login";
    return axios(URL, {
      method: "POST",
      headers: {
        "content-type": "application/json"
      },
      data: userInfo
    }).then(response => {
      const { credentialError } = response.data;
      if (credentialError) {
        dispatch(loginError(credentialError));
      } else {
        const { token } = response.data;
        const email = jwt(token).email;
        dispatch(loginSuccess({ email: email }));
        handleStorage(token);
      }
    });
  }
};

export const registerUser = userInfo => dispatch => {
  const { error } = validateUserRegister(userInfo);
  if (error) {
    dispatch(registerError(error.details[0].message));
  } else {
    const URL = "http://localhost:3002/signUp";
    return axios(URL, {
      method: "POST",
      headers: {
        "content-type": "application/json"
      },
      data: userInfo
    }).then(response => {
      const { credentialError } = response.data;
      if (credentialError) {
        dispatch(registerError(credentialError));
      } else {
        const { token } = response.data;
        const email = jwt(token).email;
        dispatch(loginSuccess({ email: email }));
        handleStorage(token);
      }
    });
  }
};
