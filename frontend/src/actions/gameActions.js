import axios from "axios";
import { userConstants } from "../constants/userConstants";
import { history } from "../helpers/history";
import { store } from "../helpers/configureStore";

const handleLogout = () => {
  store.dispatch({ type: userConstants.LOGOUT });
  history.push("/");
  return false;
};

const getActualToken = () => {
  const token = localStorage.getItem("token");
  return token;
};

const setNewToken = newToken => {
  localStorage.setItem("token", newToken);
};

export const writeToArray = payload => {
  const token = getActualToken();
  const URL = "http://localhost:3002/writeToArray";
  return axios(URL, {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    data: { data: payload, token: token }
  })
    .then(response => {
      const { data } = response;
      data ? setNewToken(data.verifiedToken) : handleLogout();
      return data;
    })
    .catch(err => {
      throw err;
    });
};

export const createNewGame = payload => {
  const token = getActualToken();
  const URL = "http://localhost:3002/createNewGame";
  return axios(URL, {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    data: { data: payload, token: token }
  })
    .then(response => {
      const { data } = response;
      data ? setNewToken(data.verifiedToken) : handleLogout();
      return data;
    })
    .catch(err => {
      throw err;
    });
};

export const repeatGame = payload => {
  const token = getActualToken();
  const URL = "http://localhost:3002/repeatGame";
  return axios(URL, {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    data: { data: payload, token: token }
  })
    .then(response => {
      const { data } = response;
      data ? setNewToken(data.verifiedToken) : handleLogout();
      return data;
    })
    .catch(err => {
      throw err;
    });
};

export const getAllGames = () => {
  const token = getActualToken();
  const URL = "http://localhost:3002/getAllGames";
  return axios(URL, {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    data: { token: token }
  })
    .then(response => {
      const { data } = response;
      data ? setNewToken(data.verifiedToken) : handleLogout();
      return data;
    })
    .catch(err => {
      throw err;
    });
};

export const getGameByID = payload => {
  const URL = "http://localhost:3002/getGameByID";
  return axios(URL, {
    method: "POST",
    headers: {
      "content-type": "application/json"
    },
    data: { data: payload }
  }).then(response => response.data);
};
