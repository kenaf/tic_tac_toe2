import { userConstants } from "../constants/userConstants";

const defaultState = {
  loggedIn: false,
  user: {},
  loginError: "",
  registerError: ""
};

const loginReducer = (state = defaultState, action) => {
  switch (action.type) {
    case userConstants.LOGINSUCCESS:
      return { loggedIn: true, ...action.payload };
    case userConstants.LOGOUT:
      localStorage.clear();
      return { loggedIn: false, user: {} };
    case userConstants.LOGINERROR:
      return { loginError: action.payload };
    case userConstants.REGISTERERROR:
      return { registerError: action.payload };
    default:
      return state;
  }
};

export default loginReducer;
