import React, { Component } from "react";
import * as ServerAPIOperations from "../actions/gameActions";
import ButtonComponent from "../components/ButtonComponent";

export default class ListOfGames extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allGames: []
    };
  }

  componentDidMount() {
    this.getAllGames();
  }

  getGameByID = idOfConnectedGame => {
    ServerAPIOperations.getGameByID({ _id: idOfConnectedGame }).then(response => {
      this.props.history.push({ pathname: "/JustPlayingGame", gameFromDb: response.connectedGame });
    });
  };

  getAllGames = () => {
    ServerAPIOperations.getAllGames().then(response => {
      const { allGames } = response;
      allGames ? this.setState({ allGames: allGames }) : this.setState({ allGames: [] });
    });
  };

  render() {
    const arrayOfAllGames = this.state.allGames.map(game => {
      return (
        <ButtonComponent
          getGameByID={this.getGameByID}
          _id={game._id}
          key={game._id}
          titleData={game.gameData.description + ", Winner number: " + game.gameData.winnerNumber}
        />
      );
    });
    return <div>{arrayOfAllGames}</div>;
  }
}
