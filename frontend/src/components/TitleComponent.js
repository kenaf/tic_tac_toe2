import React, { Component } from "react";
import "./Styles.css";

class TitleComponent extends Component {
  render() {
    const titlesForOutput = this.props.titleData.map(title => <div key={title.key}>{title.title}</div>);
    return titlesForOutput;
  }
}
export default TitleComponent;
