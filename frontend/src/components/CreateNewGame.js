import React, { Component } from "react";
import * as ServerAPIOperations from "../actions/gameActions";
import InputComponent from "../components/InputComponent";

export default class CreateNewGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameData: {
        newGameRow: 0,
        newGameColumn: 0,
        winnerNumber: 0,
        description: ""
      },
      helperText: ""
    };
  }

  createNewGame = () => {
    ServerAPIOperations.createNewGame({ gameData: this.state.gameData })
      .then(
        this.setState({
          helperText: "Game was succesfully created"
        })
      )
      .catch(error => {
        console.log(error);
      });
  };

  handleChange = values => {
    const { value, name } = values.target;
    this.setState(prevState => ({
      gameData: {
        ...prevState.gameData,
        [name]: value
      }
    }));
  };

  checkBlankObject = gameData => {
    let helperValue = 0;
    Object.values(gameData).forEach(objectValue => {
      if (objectValue === 0 || objectValue === "") helperValue++;
    });
    helperValue !== 0 ? this.setState({ helperText: "Fields are mandatory !!" }) : this.createNewGame();
  };

  render() {
    const inputsNewGame = [
      { key: "description", label: "Description", type: "text" },
      { key: "newGameRow", label: "Row " },
      { key: "newGameColumn", label: "Column " },
      { key: "winnerNumber", label: "Winner number " }
    ];
    const { helperText, gameData } = this.state;

    return (
      <div>
        <button
          onClick={() => {
            this.checkBlankObject(gameData);
          }}
        >
          Complete new game
        </button>
        <InputComponent dataForInputs={inputsNewGame} handleChange={this.handleChange} />
        {helperText}
      </div>
    );
  }
}
