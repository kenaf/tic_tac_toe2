import React, { Component } from "react";
import TitleComponent from "./TitleComponent";
import * as ServerAPIOperations from "../actions/gameActions";

export default class JustPlayingGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: 0,
      winnerText: "",
      currentSign: "x",
      playerX: 0,
      playerO: 0,
      arrayData: [],
      gameData: {
        newGameRow: 0,
        newGameColumn: 0,
        winnerNumber: 0,
        description: ""
      },
      currentRow: 0,
      currentColumn: 0,
      buttonIsVisible: false,
      helperText: ""
    };
  }

  componentDidMount() {
    this.setGameById(this.props.location.gameFromDb);
    this.intervalId = setInterval(this.getGameByID.bind(this), 500);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  setGameById = gameFromDb => {
    gameFromDb
      ? this.setState({
          _id: gameFromDb._id,
          gameData: gameFromDb.gameData
        })
      : this.setState({ helperText: "There is no game playing !!" });
  };

  getGameByID = () => {
    const { _id } = this.state;
    ServerAPIOperations.getGameByID({ _id: _id }).then(response => {
      const { connectedGame } = response;
      if (_id && connectedGame) {
        this.setState({
          arrayData: connectedGame.arrayData,
          playerO: connectedGame.playerO,
          playerX: connectedGame.playerX,
          currentSign: connectedGame.currentSign,
          buttonIsVisible: connectedGame.buttonIsVisible,
          winnerText: connectedGame.winnerText
        });
      }
    });
  };

  repeatGame = () => {
    ServerAPIOperations.repeatGame(this.state).catch(error => {
      console.log(error);
    });
  };

  writeToArray = () => {
    const { arrayData, currentRow, currentColumn, currentSign } = this.state;
    if (arrayData[currentRow - 1][currentColumn - 1] !== currentSign) {
      ServerAPIOperations.writeToArray(this.state).catch(error => {
        console.log(error);
      });
    }
  };

  render() {
    const repeatGameButton = this.state.buttonIsVisible ? (
      <button type="button" onClick={this.repeatGame}>
        Repeat game
      </button>
    ) : null;

    const arrayForOutput = (
      <div>
        {this.state.arrayData.map((row, rowIndex) => {
          return (
            <div key={rowIndex}>
              {row.map((column, columnIndex) => {
                return (
                  <button
                    className="button"
                    value={column}
                    key={columnIndex}
                    onClick={() => {
                      this.setState({ currentRow: rowIndex + 1, currentColumn: columnIndex + 1 }, () => this.writeToArray());
                    }}
                  >
                    {column}
                  </button>
                );
              })}
            </div>
          );
        })}
      </div>
    );

    const titlesGameState = [{ key: "title", title: "State of the Game: " }];
    const { playerX, playerO, winnerText, helperText } = this.state;

    return (
      <div>
        <TitleComponent titleData={titlesGameState} />
        <div>Player X: {playerX}</div>
        <div>Player O: {playerO}</div>
        <br />
        {winnerText}
        {repeatGameButton}
        {arrayForOutput}
        {helperText}
      </div>
    );
  }
}
