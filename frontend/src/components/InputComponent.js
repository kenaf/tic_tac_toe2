import React, { Component } from "react";

class InputComponent extends Component {
  switchTypes = type => {
    switch (type) {
      case "password":
        return "password";
      case "number":
        return "number";
      default:
        return "text";
    }
  };

  render() {
    const { dataForInputs, handleChange } = this.props;
    const inputsForOutput = dataForInputs.map(input => (
      <form key={input.label}>
        <label>{input.label}</label>
        <input
          style={{ width: "100px" }}
          key={input.key}
          value={dataForInputs[input.key]}
          name={input.key}
          type={this.switchTypes(input.type)}
          onChange={currentValue => {
            handleChange(currentValue);
          }}
          autoComplete="on"
        />
      </form>
    ));
    return inputsForOutput;
  }
}
export default InputComponent;
