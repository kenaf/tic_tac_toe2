import React from "react";

const ButtonComponent = props => {
  return <button onClick={() => props.getGameByID(props._id)}>{props.titleData}</button>;
};
export default ButtonComponent;
