import React from "react";
import CreateNewGame from "./CreateNewGame";
import ListOfGames from "./ListOfGames";
import JustPlayingGame from "./JustPlayingGame";

import { connect } from "react-redux";
import { history } from "../helpers/history";
import { store } from "../helpers/configureStore"; //when need to use store.getState()
import { logout } from "../../src/actions/userActions";

import Menu, { MenuItem } from "rc-menu";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "rc-menu/assets/index.css";
import "bootstrap/dist/css/bootstrap.min.css";

class HomePage extends React.Component {
  handleLogout = () => {
    this.props.logout();
    history.push("/");
  };

  render() {
    const userEmail = store.getState("user").email;
    return (
      <Router>
        <Menu mode="horizontal">
          <MenuItem>
            <Link to="CreateNewGame" style={{ textDecoration: "none" }}>
              Create new game
            </Link>
          </MenuItem>
          <MenuItem>
            <Link to="/ListOfGames" style={{ textDecoration: "none" }}>
              List of games
            </Link>
          </MenuItem>
          <MenuItem>
            <Link to="/JustPlayingGame" style={{ textDecoration: "none" }}>
              Just playing game
            </Link>
          </MenuItem>
          <MenuItem onClick={this.handleLogout} style={{ float: "right" }}>
            <Link to="">Logout</Link>
          </MenuItem>
          <MenuItem style={{ float: "right" }}>
            <Link to="">{userEmail}</Link>
          </MenuItem>
        </Menu>
        <Switch>
          <Route path="/CreateNewGame" component={CreateNewGame} />
          <Route path="/ListOfGames" component={ListOfGames} />
          <Route path="/JustPlayingGame" component={JustPlayingGame} />
        </Switch>
      </Router>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout())
  };
};

export default connect(null, mapDispatchToProps)(HomePage);
