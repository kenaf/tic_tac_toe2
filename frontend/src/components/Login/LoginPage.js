import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../actions/userActions";
import InputComponent from "../InputComponent";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: "",
        password: ""
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    });
  }

  handleSubmit() {
    const { user, deviceId } = this.state;
    this.props.loginUser(user, deviceId);
  }

  render() {
    const inputsForLogin = [
      { key: "email", label: "Email", type: "text" },
      { key: "password", label: "Password ", type: "password" }
    ];

    return (
      <div>
        <h2>Login</h2>
        <InputComponent dataForInputs={inputsForLogin} handleChange={this.handleChange} />
        <button onClick={this.handleSubmit} type="button">
          Login
        </button>
        <br />
        <a href="/register">Don't have an account</a>
        <br />
        {this.props.loginError}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginError: state.loginError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginUser: bindActionCreators(userActions.loginUser, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
