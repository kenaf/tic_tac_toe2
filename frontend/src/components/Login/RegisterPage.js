import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../actions/userActions";
import InputComponent from "../InputComponent";

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        email: "",
        password: "",
        confirmPassword: ""
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    });
  }

  handleSubmit() {
    this.props.registerUser(this.state.user);
  }

  render() {
    const inputsForRegister = [
      { key: "email", label: "Email", type: "text" },
      { key: "password", label: "Password ", type: "password" },
      { key: "confirmPassword", label: "Confirm Password ", type: "password" }
    ];
    return (
      <div>
        <h2>Create account</h2>
        <InputComponent dataForInputs={inputsForRegister} handleChange={this.handleChange} />
        <button onClick={this.handleSubmit} type="button">
          Create
        </button>
        <br />
        <a href="/login">Do you already have an account</a>
        <div>{this.props.registerError}</div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    registerError: state.registerError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    registerUser: bindActionCreators(userActions.registerUser, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
