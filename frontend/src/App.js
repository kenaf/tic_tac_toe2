import React from "react";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import LoginPage from "./components/Login/LoginPage";
import RegisterPage from "./components/Login/RegisterPage";
import HomePage from "./components/HomePage";
import { PrivateRoute } from "./components/Login/PrivateRoute";
import { history } from "./helpers/history";

export default class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <PrivateRoute exact path="/" component={HomePage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Redirect from="*" to="/" />
        </Switch>
      </Router>
    );
  }
}
