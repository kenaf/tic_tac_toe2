const config = {
  keyService: { port: 6379, host: "localhost" },
  jwt: { algorithm: "HS256", expirationTime: 11 },
  signUp: { saltRounds: 10 },
  signOptions: { issuer: "Papers a.s.", subject: "papers@info.com", audience: "https://papers.com" },
  games: {
    winnerText: "You won, do you want to repeat game?",
    differentEmail: "Try different email/password.",
    userDoesntExist: "The user doesn't exists.",
    userAlreadyExist: "User with this email already exists."
  }
};
module.exports = config;
