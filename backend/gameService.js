"use strict";
const gameRepository = require("./gameRepository");
const KeyService = require("./redis/KeyService");
const config = require("./config");

handleToken = async token => {
  const tokenFromRedis = await KeyService.get(token);
  if (tokenFromRedis) {
    const verifiedToken = await KeyService.verify(tokenFromRedis);
    if (verifiedToken) {
      KeyService.delete(token);
      const renewedToken = await KeyService.set(verifiedToken.email);
      return renewedToken;
    } else {
      return false;
    }
  } else {
    return false;
  }
};

getEmptyArray = (row, column) => {
  const arrayData = [];
  for (let rowLoop = 0; rowLoop < row; rowLoop++) {
    arrayData[rowLoop] = [];
    for (let columnLoop = 0; columnLoop < column; columnLoop++) {
      arrayData[rowLoop][columnLoop] = "*";
    }
  }
  return arrayData;
};

const getAllGames = async games => {
  const verifiedToken = await handleToken(games.token);
  if (verifiedToken) {
    const allGames = await gameRepository.getAllGames();
    return { allGames: allGames, verifiedToken: verifiedToken };
  } else return false;
};

const createNewGame = async newGameData => {
  const verifiedToken = await handleToken(newGameData.token);
  if (verifiedToken) {
    const gameData = newGameData.data.gameData;
    const arrayData = getEmptyArray(gameData.newGameRow, gameData.newGameColumn);
    const gameToCreate = { gameData, arrayData, playerO: 0, playerX: 0 };
    const newGameId = await gameRepository.createNewGame(gameToCreate);
    return { newGameId: newGameId, arrayData: arrayData, verifiedToken: verifiedToken };
  } else return false;
};

const getGameByID = async game => {
  const { _id } = game.data;
  const connectedGame = await gameRepository.getGameByID(_id);
  return { connectedGame: connectedGame };
};

const writeToArray = async writtenData => {
  const verifiedToken = await handleToken(writtenData.token);
  if (verifiedToken) {
    const { newGameRow, newGameColumn, winnerNumber } = writtenData.data.gameData;
    const { currentRow, currentColumn, arrayData, _id } = writtenData.data;
    const winnerText = config.games.winnerText;
    let { playerO, playerX, signCounter = 0, isWinner = false } = writtenData.data;
    let currentSign = writtenData.data.currentSign === "X" ? "O" : "X";
    arrayData[currentRow - 1][currentColumn - 1] = currentSign;

    const checkCounter = (signCounter, sign) => {
      if (signCounter == winnerNumber) {
        sign === "O" ? playerO++ : playerX++;
        isWinner = true;
      }
    };

    //checking current row
    for (currentRow - 1, column = 0; column < newGameColumn; column++) {
      arrayData[currentRow - 1][column] === currentSign ? signCounter++ : (signCounter = 0);
      checkCounter(signCounter, currentSign);
    }
    signCounter = 0; //checking current column
    for (let row = 0, column = currentColumn - 1; row < newGameRow; row++) {
      arrayData[row][column] === currentSign ? signCounter++ : (signCounter = 0);
      checkCounter(signCounter, currentSign);
    }
    signCounter = 0; //checking diagonal from left up to right down
    if (currentRow - currentColumn < 0 || currentRow - currentColumn === 0) {
      const absoluteValue = Math.abs(currentRow - currentColumn);
      for (let row = 0, column = absoluteValue; row < newGameRow && column < newGameColumn; row++, column++) {
        arrayData[row][column] === currentSign ? signCounter++ : (signCounter = 0);
        checkCounter(signCounter, currentSign);
      }
      signCounter = 0;
    } else {
      for (let row = currentRow - currentColumn, column = 0; row < newGameRow && column < newGameColumn; row++, column++) {
        arrayData[row][column] === currentSign ? signCounter++ : (signCounter = 0);
        checkCounter(signCounter, currentSign);
      }
      signCounter = 0;
    } //checking diagonal from left down to right  up
    if (currentRow + currentColumn < newGameRow) {
      for (let row = currentRow + currentColumn, column = 0; row >= 0 && column < newGameColumn; row--, column++) {
        arrayData[row][column] === currentSign ? signCounter++ : (signCounter = 0);
        checkCounter(signCounter, currentSign);
      }
      signCounter = 0;
    } else {
      const startPosition = newGameColumn - (newGameColumn - currentColumn + (newGameRow - currentRow)) - 1;
      for (let row = newGameRow - 1, column = startPosition; row >= 0 && column < newGameColumn; row--, column++) {
        arrayData[row][column] === currentSign ? signCounter++ : (signCounter = 0);
        checkCounter(signCounter, currentSign);
      }
      signCounter = 0;
    }

    if (isWinner) {
      gameRepository.writeToArrayWinner({ _id, arrayData, playerO, playerX, winnerText, buttonIsVisible: true });
      return { arrayData: arrayData, currentSign: currentSign, verifiedToken: verifiedToken };
    } else {
      gameRepository.writeToArrayNoWinner({ _id, arrayData, currentSign });
      return { changedArrayData: arrayData, currentSign: currentSign, verifiedToken: verifiedToken };
    }
  } else return false;
};

const repeatGame = async repeatData => {
  const verifiedToken = await handleToken(repeatData.token);
  if (verifiedToken) {
    const { newGameRow, newGameColumn } = repeatData.data.gameData;
    const arrayData = getEmptyArray(newGameRow, newGameColumn);
    await gameRepository.repeatGame({ repeatData: repeatData.data, arrayData });
    return { verifiedToken: verifiedToken };
  } else return false;
};

module.exports = { getAllGames, createNewGame, getGameByID, writeToArray, repeatGame };
