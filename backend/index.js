const cors = require("../frontend/node_modules/@koa/cors");
const Koa = require("./node_modules/koa");
const game = require("./gameConnector");
const user = require("./Login/userConnector");
const bodyParser = require("../frontend/node_modules/koa-bodyparser");
const app = new Koa();

app.use(cors());
app.use(bodyParser());
app.use(game);
app.use(user);

app.listen(3002, () => {
  console.log("Game server is running on localhost:3002");
});
