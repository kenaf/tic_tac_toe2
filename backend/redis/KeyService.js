const redis = require("redis");
const Promise = require("bluebird");
const config = require("../config");
const { sign, decode, verify } = require("../Login/handleJwt");

Promise.promisifyAll(redis.RedisClient.prototype);

function KeyService() {
  const {
    keyService: { port, host }
  } = config;
  this.client = redis.createClient(port, host);
  this.client.on("connect", function() {
    console.log("Redis is running");
  });
  console.log("Connecting to Redis...");
}

// Decode JWT token, find in Redis by signature and return the token
KeyService.prototype.get = async function(token) {
  const signature = decode(token).signature;
  const tokenFromRedis = await this.client.getAsync(signature);
  return tokenFromRedis;
};

// Generate and store a new JWT token like key: signature, value: token
KeyService.prototype.set = function(email) {
  const token = sign({ email }, config.signOptions);
  const signature = decode(token).signature;
  this.client.set(signature, token);
  return token;
};

// Verify token and return the token or false
KeyService.prototype.verify = function(token) {
  const verifiedToken = verify(token, config.signOptions);
  return verifiedToken;
};

// Delete token in redis
KeyService.prototype.delete = function(token) {
  const signature = decode(token).signature;
  this.client.del(signature);
};

module.exports = new KeyService();
