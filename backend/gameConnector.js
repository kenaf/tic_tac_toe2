const Router = require("../frontend/node_modules/koa-router");
const router = new Router();
const gameService = require("./gameService");

router.post("/getAllGames", async ctx => {
  const allGames = await gameService.getAllGames(ctx.request.body);
  ctx.body = allGames;
});

router.post("/createNewGame", async ctx => {
  const newGameId = await gameService.createNewGame(ctx.request.body);
  ctx.body = newGameId;
});

router.post("/getGameByID", async ctx => {
  const connectedGame = await gameService.getGameByID(ctx.request.body);
  ctx.body = connectedGame;
});

router.post("/writeToArray", async ctx => {
  const writtenData = await gameService.writeToArray(ctx.request.body);
  ctx.body = writtenData;
});

router.post("/repeatGame", async ctx => {
  const repeatedGame = await gameService.repeatGame(ctx.request.body);
  ctx.body = repeatedGame;
});

module.exports = router.middleware();
