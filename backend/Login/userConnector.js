const Router = require("../../frontend/node_modules/koa-router");
const router = new Router();
const userService = require("./userService");

router.post("/login", async ctx => {
  const token = await userService.login(ctx.request.body);
  ctx.body = token;
});

router.post("/signUp", async ctx => {
  const token = await userService.signUp(ctx.request.body);
  ctx.body = token;
});

module.exports = router.middleware();
