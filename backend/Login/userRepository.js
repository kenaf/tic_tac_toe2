const getConnection = require("../handleDBConnection").getConnection;

const findUserByEmail = async userData => {
  const db = await getConnection();
  const userFromDb = await db.collection("users").findOne({ email: userData.userData.email });
  return userFromDb;
};

const createUser = async userData => {
  const db = await getConnection();
  const email = { email: userData.email };
  const newUser = await db.collection("users").findOneAndUpdate(email, { $set: userData }, { upsert: true });
  return newUser.lastErrorObject.updatedExisting;
};

module.exports = {
  findUserByEmail,
  createUser
};
