const fs = require("fs");
const jwt = require("jsonwebtoken");
const config = require("../config");
const jwt_encoding_algorithm = config.jwt.algorithm;
const expiration_time = config.jwt.expirationTime;
const privateKEY = fs.readFileSync("./Login/private.key", "utf8");

module.exports = {
  sign: (payload, $Options) => {
    const signOptions = {
      issuer: $Options.issuer,
      subject: $Options.subject,
      audience: $Options.audience,
      expiresIn: expiration_time,
      algorithm: jwt_encoding_algorithm
    };
    return jwt.sign(payload, privateKEY, signOptions);
  },
  verify: (token, $Option) => {
    const verifyOptions = {
      issuer: $Option.issuer,
      subject: $Option.subject,
      audience: $Option.audience,
      expiresIn: expiration_time,
      algorithm: [jwt_encoding_algorithm]
    };

    try {
      return jwt.verify(token, privateKEY, verifyOptions);
    } catch (err) {
      return false;
    }
  },
  //returns null if token is invalid
  decode: token => {
    return jwt.decode(token, { complete: true });
  }
};
