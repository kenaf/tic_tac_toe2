"use strict";
const bcrypt = require("bcrypt");
const redis = require("redis");
const config = require("../config");
client = redis.createClient();
const userRepository = require("./userRepository");
const KeyService = require("../redis/KeyService");

const login = async userData => {
  const dbUserData = await userRepository.findUserByEmail({ userData });
  const { differentEmail, userDoesntExist } = config.games;
  if (dbUserData) {
    const { password, email } = dbUserData;
    const validatePassword = await bcrypt.compare(userData.password, password);
    if (!validatePassword) {
      return { credentialError: differentEmail };
    } else {
      const accessToken = KeyService.set(email);
      return { token: accessToken };
    }
  } else {
    return { credentialError: userDoesntExist };
  }
};

const signUp = async userData => {
  const { email } = userData;
  const { saltRounds } = config.signUp;
  const { userAlreadyExist } = config.games;
  const hashedPswd = await bcrypt.hash(userData.password, saltRounds);
  const newUser = await userRepository.createUser({ email: email, password: hashedPswd });
  if (newUser) {
    return { credentialError: userAlreadyExist };
  } else {
    const accessToken = KeyService.set(email);
    return { token: accessToken };
  }
};

module.exports = { login, signUp };
