const getConnection = require("./handleDBConnection").getConnection;
const ObjectID = require("mongodb").ObjectID;

const getAllGames = async () => {
  const db = await getConnection();
  const allGamesFromDb = await db
    .collection("gameData")
    .find()
    .toArray({});
  return allGamesFromDb;
};

const createNewGame = async gameToCreate => {
  const db = await getConnection();
  const newGameId = await db.collection("gameData").insertOne(gameToCreate);
  return newGameId.ops[0]._id;
};

const getGameByID = async connectedGameId => {
  const db = await getConnection();
  const gameFromDb = await db.collection("gameData").findOne({ _id: ObjectID(connectedGameId) });
  return gameFromDb;
};

const writeToArrayWinner = async writtenData => {
  const db = await getConnection();
  db.collection("gameData").updateOne(
    { _id: ObjectID(writtenData._id) },
    {
      $set: { arrayData: [], playerO: writtenData.playerO, playerX: writtenData.playerX, winnerText: writtenData.winnerText, buttonIsVisible: true }
    },
    { upsert: true },
    function(err) {
      if (err) throw err;
    }
  );
};

const writeToArrayNoWinner = async writtenData => {
  const db = await getConnection();
  db.collection("gameData").updateOne(
    { _id: ObjectID(writtenData._id) },
    { $set: { arrayData: writtenData.arrayData, currentSign: writtenData.currentSign } },
    { upsert: true },
    function(err) {
      if (err) throw err;
    }
  );
};

const repeatGame = async repeatData => {
  const db = await getConnection();
  db.collection("gameData").updateOne(
    { _id: ObjectID(repeatData.repeatData._id) },
    { $set: { gameData: repeatData.repeatData.gameData, arrayData: repeatData.arrayData, winnerText: "", buttonIsVisible: false } },
    { upsert: true },
    function(err) {
      if (err) throw err;
    }
  );
};

module.exports = {
  getAllGames,
  createNewGame,
  getGameByID,
  writeToArrayWinner,
  writeToArrayNoWinner,
  repeatGame
};
