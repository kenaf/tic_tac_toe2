const DB_NAME = "TicTacToe";
const DB_URL = "mongodb+srv://michal:Arletka@cluster0-drryd.mongodb.net/TicTacToe?retryWrites=true&w=majority";
const { MongoClient } = require("../frontend/node_modules/mongodb");
let db;

const getConnection = async () => {
  if (!db) {
    const client = new MongoClient(DB_URL, { useUnifiedTopology: true });
    try {
      await client.connect();
      db = client.db(DB_NAME);
    } catch (e) {
      console.error(e);
    }
    db.close;
  }
  return db;
};

module.exports = {
  getConnection
};
